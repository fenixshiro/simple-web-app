﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApplicationScoreboard.Models;

namespace WebApplicationScoreboard.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            List<PlayerModel> scoreboard = new List<PlayerModel>();

            using (var db = new WebAppContext())
            {
                scoreboard = db.Scoreboard.ToList();//Получение списка из базы данных
            }

            scoreboard.Sort((x, y) => y.Score.CompareTo(x.Score));//Сортировка по счету

            ViewBag.scoreboard = scoreboard;//Передача списка в представление

            return View();
        }

        public IActionResult Edit()
        {
            List<PlayerModel> scoreboard = new List<PlayerModel>();

            using (var db = new WebAppContext())
            {
                scoreboard = db.Scoreboard.ToList();//Получение списка из базы данных
            }

            scoreboard.Sort((x, y) => y.Score.CompareTo(x.Score));//Сортировка по счету

            ViewBag.scoreboard = scoreboard;//Передача списка в представление

            return View();
        }
        [HttpPost]
        public IActionResult Edit( PlayerModel player )
        {
            if( player.Id == 0)
            {
                using (var db = new WebAppContext())
                {
                    db.Add(player);//Добавление записи в базу данных
                    db.SaveChanges();//Сохранение изменений
                }
            }

            if(player.Id > 0 && player.Score != 0)
            {
                using (var db = new WebAppContext())
                {
                    var UpdatePlayer = db.Scoreboard.Where(ply => ply.Id == player.Id).FirstOrDefault();

                    UpdatePlayer.Nickname = player.Nickname;
                    UpdatePlayer.Score = player.Score;
                    db.SaveChanges();
                }
            }

            if(player.Id > 0 && player.Score == 0)
            {
                using (var db = new WebAppContext())
                {
                    db.Attach(player);
                    db.Remove(player);
                    db.SaveChanges();
                }
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}