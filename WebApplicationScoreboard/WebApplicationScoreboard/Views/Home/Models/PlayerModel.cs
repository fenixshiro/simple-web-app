﻿namespace WebApplicationScoreboard.Models
{
    public class PlayerModel
    {
        public int Id { get; set; }
        public string Nickname { get; set; }
        public int Score { get; set; }
    }
}
