﻿using Microsoft.EntityFrameworkCore;

namespace WebApplicationScoreboard.Models
{
    public class WebAppContext : DbContext
    {
        public DbSet<PlayerModel> Scoreboard { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
            => options.UseSqlite(@"Data Source=E:\Users\Univer\files\Scoreboard.db");
    }
}
